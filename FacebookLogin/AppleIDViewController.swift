//
//  AppleIDViewController.swift
//  FacebookLogin
//
//  Created by Nyta on 11/27/20.
//

import UIKit
import AuthenticationServices

class AppleIDViewController: UIViewController {

    let appleProvider = AppleSignInClient()
    let appleBtn = ASAuthorizationAppleIDButton()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @objc func signInWithApple(sender: ASAuthorizationAppleIDButton) {
        appleProvider.handleAppleIdRequest(block: { fullName, email, token in
            // receive data in login class.
})

    }
    
    @IBAction func btnAppleID(_ sender: Any) {
        appleBtn.addTarget(self, action: #selector(signInWithApple(sender: )), for: .touchUpInside)
        appleBtn.sendActions(for: .touchUpInside)
    }
    
}
