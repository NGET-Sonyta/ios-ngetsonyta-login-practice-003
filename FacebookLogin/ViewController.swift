
import UIKit
import FBSDKLoginKit

class ViewController: UIViewController{

    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var emailAddress: UILabel!
    
    
    
    let loginButton = FBLoginButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if let token = AccessToken.current,
               !token.isExpired {
            func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
                let token = token.tokenString

                let request = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                         parameters: ["fields": "email,name, picture"],
                                                         tokenString: token, version: nil, httpMethod: .get)
                request.start(completionHandler: {connetion, result, error in
                    print("\(String(describing: result))")
                    print("View Didload")
        })
    }
}
        
            loginButton.delegate = self
            loginButton.permissions = ["public_profile", "email"]
        
    }
  
    @IBAction func login(_ sender: UIButton) {
        loginButton.sendActions(for: .touchUpInside)
    }
    
    @IBAction func loginPhone(_ sender: UIButton) {
//        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
//          if let error = error {
//            self.showMessagePrompt(error.localizedDescription)
//            print(verificationID)
//
//            return
//          }
          // Sign in using the verificationID and the code sent to the user
          // ...
        //}
//
//        let demoViewController = PhoneViewController()
//            //demoViewController.delegate = self
//            self.present(demoViewController, animated: true, completion: nil)
//          }
    }
    
    @IBAction func sendPhone(_ sender: Any) {
        
        print("Phone")
//        guard let phoneNumber = phone.text else {
//            return
//        }
//        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
//                  if let error = error {
//                    self.showMessagePrompt(error.localizedDescription)
//                    print(verificationID)
//        
//                    return
//                  }
        
    }
    @IBAction func loginApple(_ sender: UIButton) {
    }
}

extension ViewController : LoginButtonDelegate{
   
    
    
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        let token = result?.token?.tokenString
        
        
        let request = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                 parameters: ["fields": "email, name, picture.type(large)"],
                                                 tokenString: token, version: nil, httpMethod: .get)
        request.start(completionHandler: {connetion, result, error in
            
            let object = result as? [String: Any]
            
            let userName = (object!["name"]! as! String)
            let email = (object!["email"]! as! String)
            let profilePic = object!["picture"] as? [String: Any]
            let profile = profilePic!["data"] as? [String: Any]
            let pic = profile!["url"]

            
            let ss = URL(string: pic as! String)
            
            let dataImg = try? Data(contentsOf: ss!)
            let image = UIImage(data: (dataImg)!)
            self.img.image = image

            self.emailAddress.text = "Email: \(email)"
            self.userName.text = "Username: \(userName)"

        })
        
        
    }
    
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
         
    }
}
