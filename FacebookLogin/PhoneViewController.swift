//
//  PhoneViewController.swift
//  FacebookLogin
//
//  Created by Nyta on 11/26/20.
//

import UIKit
import FirebaseCore
import FirebaseAuth
class PhoneViewController: UIViewController {

  
    var verification_id: String? = nil

    
    @IBOutlet weak var verify: UITextField!
    @IBOutlet weak var phone: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        

//        verify.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnSend(_ sender: UIButton) {
        
        print("hello")

       
        if !verify.isHidden {
                    if !phone.text!.isEmpty{
                        Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                           PhoneAuthProvider.provider().verifyPhoneNumber(phone.text!, uiDelegate: nil, completion: {
                               verificationID, error in
                              if(error != nil){
                                   return
                               }
                               else{
               
                                  self.verification_id = verificationID
                                  self.verify.isHidden = false
                               }
                             })
                    }
                    else{
                        print("Please Enter Your Mobile Number")
                    }
                }else{
                    if verification_id != nil{
                        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification_id!, verificationCode: verify.text!)
                        Auth.auth().signIn(with: credential, completion: {authData, error in
                            if (error != nil){
                                print(error.debugDescription)
                            }
                            else{
                                print("Authentication Success With - " + (authData?.user.phoneNumber! ?? "No Phone Number"))
                            }
                        })
                    }
                    else{
                        print("Error In Getting Verification ID")
                    }
                 }

    }
        }

